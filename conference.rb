require_relative 'section'
class Conference

  def self.organize_tracks (lectures)
    @tracks_name = ('a'..'z').to_a

    @lectures = lectures
    @sections = Section.create_sections
    morning_sessions = organize_lectures @sections, true
    afternoon_sessions = organize_lectures @sections, false
    tracks = prepare_tracks morning_sessions, afternoon_sessions

    display_tracks tracks

  end


  def self.prepare_tracks (morning_sessions, afternoon_sessions)
    tracks  = []
    sessions_days = morning_sessions.length
    while sessions_days > 0
      tracks.push([morning_sessions.shift,afternoon_sessions.shift])
      sessions_days -= 1
    end
    tracks
  end

  def self.display_tracks(tracks)
    tracks.each_with_index do |sessions,index|
      print 'Track ' +@tracks_name[index].to_s.upcase+ "\n"
      time = Time.now.change({ hour: 9, min: 0, sec: 0 })
      sessions.each_with_index do |value,key|
        value.each do |t|
          print t.get_title(time)
          time = time + t.time*60
        end
        if key == 0
          print time.strftime('%H:%M') + " Almoço \n"
          time = Time.now.change({ hour: 13, min: 0, sec: 0 })
        else
          print time.strftime('%H:%M') + " Evento de Networking  \n"
        end

      end
    end
  end

  def self.organize_lectures(sections,morning)
    lectures = []
    sections.each do |section|
      if morning && section.min_time == section.max_time
        result = (get_lectures [], section.min_time,section.max_time)
        while get_total_duration(result) != section.max_time
          @lectures = send_lecture_to_end result.first,result, @lectures
          result = get_lectures result, section.min_time, section.max_time
        end
        @sections.delete section

      elsif !morning
        result = (get_lectures [], section.min_time, section.max_time)
       while !get_total_duration(result).between? section.min_time, section.max_time
          result.push @lectures.shift
       end

      end
      lectures.push result

    end
    lectures
  end


  ## get lectures for each session
  def self.get_lectures (lectures_final, min_time, max_time)
    @lectures.each do |lecture|
      add_lecture = get_total_duration(lectures_final) + lecture.time
      if Section.can_add add_lecture, max_time, min_time
        lectures_final.push lecture
        @lectures.delete(lecture)
      else
        @lectures = send_lecture_to_end lecture, @lectures, @lectures
      end
    end
    lectures_final
  end

  ## move the lecture to end of array
  def self.send_lecture_to_end (lecture,remove_from, destination)
    remove_from.delete(lecture)
    destination.push(lecture)
    destination
  end

 ## return total time of lectures
  def self.get_total_duration (lectures)
    total = 0
    lectures.each do |lecutre|
      total += lecutre.time
    end
    total
  end


end
