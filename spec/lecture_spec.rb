require_relative '../lecture'

RSpec.describe Lecture , "#lecture" do
  context "organize lecture" do
    it "can get title from line" do
      line = 'lecture start 30min'
      title = Lecture.get_lecture_title line
      expect(title).to eq 'lecture start'
    end
    it "can get time from line" do
      line = "lecture start 30min"
      time = Lecture.get_lecture_time line
      expect(time).to eq 30
    end

    it "can create array of lectures from text file" do
        lectures = Lecture.get_lectures_from_txt
        expect(lectures.length).to eq 19
        expect(lectures[5].time).to eq (5)
    end

    it "can create array of lectures from text file" do
      lectures = Lecture.get_lectures_from_txt
      expect(lectures.length).to eq 19
      expect(lectures[5].time).to eq (5)
    end

    it "can get lecture time string" do
      lecture = 'lecture start 30min'
      expect(Lecture.get_lecture_time_string lecture).to eq '30min'
    end
  end

end