require 'rspec'
require_relative '../section'

RSpec.describe Section ,"#section" do
  context 'write sections to conference' do

    it 'should create the sections' do

      sections = Section.create_sections
      expect(sections.length).to eq 4
      expect(sections.first.min_time).to eq 180
      expect(sections.first.max_time).to eq 180
      expect(sections[1].max_time).to eq 240
      expect(sections.last.min_time).to eq 180
      expect(sections.last.max_time).to eq 240
    end

    it 'should add more tracks' do
      total = 0
      min = 180
      max =180

      expect(Section.can_add(total,max,min)).to eq true
      total = 100
      max =240
      expect(Section.can_add(total,max,min)).to eq true
      total = 200
      expect(Section.can_add(total,max,min)).to eq false

    end
  end
end