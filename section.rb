class Section

  attr_accessor  :min_time, :max_time

  def initialize(min_time, max_time  )
    @min_time = min_time
    @max_time = max_time
  end

  def self.create_sections
    sections = []
    is_morning = true
    num_sections = 4

    while num_sections > 0
      if is_morning
        section = Section.new 180,180
      else
        section = Section.new 180,240
      end
      is_morning = ! is_morning
      sections.push section
      num_sections -= 1
    end

    sections
  end


  def self.can_add (total,max_time,min_time)
    if max_time == min_time
     result = total <= max_time
    else
      result = !total.between?(min_time, max_time)
    end
    result
  end
end