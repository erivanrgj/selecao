class Lecture

  attr_accessor :title, :time, :time_string

  def initialize(title, time, time_string)
    @title = title
    @time = time
    @time_string = time_string
  end

  def get_title (time)
    time.strftime('%H:%M') + ' '+ self.title.to_s +  ' ' + self.time_string.to_s + " \n"
  end
  
  def self.get_lectures_from_txt
    lectures = []
    File.open("proposals.txt", "r") do |f|
      f.each_line do |line|
        time = get_lecture_time line
        time_string = get_lecture_time_string line
        title = get_lecture_title line

        lecture = self.new title,time,time_string
        lectures.push lecture
      end
    end
    lectures
  end

  def self.get_lecture_time_string line
    time = /\d+min|lightning/.match(line)
    time[0]
  end

  def self.get_lecture_title line
    line.sub(/\d+min|lightning/, '').strip
  end
  ## get time from each line
  def self.get_lecture_time line
    time = /\d+|lightning/.match(line)
    if time[0] == 'lightning'
      return 5
    end
    time[0].to_i unless time.nil?
  end


end