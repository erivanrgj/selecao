# lib/tasks/import_topics.rake
task :organize_tracks do
  lectures =  Lecture.get_lectures_from_txt
  Conference.organize_tracks lectures
end